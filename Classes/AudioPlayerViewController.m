//
//  Created by Eric Stockmeyer on 9/23/14.
//
//

#import "AudioPlayerViewController.h"

@implementation AudioPlayerViewController


- (void)viewDidLoad {
    [super viewDidLoad];

	NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/pumpupthespeed.mp3", [[NSBundle mainBundle] resourcePath]]];
	
	NSError *error;
	audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
	audioPlayer.numberOfLoops = -1;
    audioPlayer.volume = 0.4f;
    audioPlayer.enableRate=YES;
    [audioPlayer prepareToPlay];
    [audioPlayer setNumberOfLoops:0];
    audioPlayer.rate=0.5f;
    //[player play];
	
	if (audioPlayer == nil)
		NSLog([error description]);				
	else 
		[audioPlayer play];

}

- (void)dealloc {
	[audioPlayer release];
    [_speedSlider release];
    [_speedLabel release];
    [_volumeSlider release];
    [_volumeLabel release];
    [super dealloc];
}

- (IBAction)speedChanged:(id)sender {
    self.speedLabel.text = [NSString stringWithFormat:@"Speed: "@"%f", self.speedSlider.value];
    audioPlayer.rate=self.speedSlider.value;
}

- (IBAction)volumeChanged:(id)sender {
    self.volumeLabel.text = [NSString stringWithFormat:@"Speed: "@"%f", self.volumeSlider.value];
    audioPlayer.volume=self.volumeSlider.value;

}
@end
