//
//  AudioPlayerAppDelegate.h
//  AudioPlayer
//
//  Created by Eric Stockmeyer on 9/23/14.
//
//


#import <UIKit/UIKit.h>

@class AudioPlayerViewController;

@interface AudioPlayerAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    AudioPlayerViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet AudioPlayerViewController *viewController;

@end

