//
//  AudioPlayerAppDelegate.m
//  AudioPlayer
//
//  Created by Eric Stockmeyer on 9/23/14.
//
//

#import "AudioPlayerAppDelegate.h"
#import "AudioPlayerViewController.h"

@implementation AudioPlayerAppDelegate

@synthesize window;
@synthesize viewController;


- (void)applicationDidFinishLaunching:(UIApplication *)application {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
