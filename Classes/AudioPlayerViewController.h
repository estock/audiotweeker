//
//  Created by Eric Stockmeyer on 9/23/14.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioPlayerViewController : UIViewController {
	AVAudioPlayer *audioPlayer;
}
@property (retain, nonatomic) IBOutlet UISlider *speedSlider;
@property (retain, nonatomic) IBOutlet UILabel *speedLabel;
@property (retain, nonatomic) IBOutlet UISlider *volumeSlider;
@property (retain, nonatomic) IBOutlet UILabel *volumeLabel;
- (IBAction)speedChanged:(id)sender;
- (IBAction)volumeChanged:(id)sender;

@end

